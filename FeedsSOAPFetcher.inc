<?php
/**
 * @file
 * FeedsSOAPFetcher and related classes.
 */

/**
 * Result of FeedsSOAPFetcher::fetch().
 */
class FeedsSOAPResult extends FeedsFetcherResult {

  protected $config;
  protected $client;

  /**
   * Override parent::__construct().
   */
  public function __construct($config) {
    $this->config = $config;

    parent::__construct();
  }

  /**
   * Get SOAP client.
   */
  protected function getClient() {
    if (!isset($this->client)) {
      $options = array();

      if (!$this->config['wsdl']) {
        if (empty($this->config['namespace'])) {
          throw new Exception(t('Target namespace URI is required when using non-WSDL mode.'));
        }

        $options['location'] = $this->config['endpoint'];
        $options['uri'] = $this->config['namespace'];

        // The style and use options only work in non-WSDL mode, in WSDL mode they come from the WSDL file.
        $options['use'] = $this->config['use'] == 0 ? SOAP_ENCODED : SOAP_LITERAL;
        $options['style'] = $this->config['style'] == 0 ? SOAP_RPC : SOAP_DOCUMENT;
      }

      // Check http://bugs.php.net/bug.php?id=36226 for more details.
      $options['features'] = SOAP_SINGLE_ELEMENT_ARRAYS;

      // Turn on tracing so we can use the full XML response to query with for example xpath.
      $options['trace'] = TRUE;

      $this->client = new SoapClient(($this->config['wsdl'] ? $this->config['endpoint'] : NULL), $options);
    }

    return $this->client;
  }

  /**
   * Get SOAP function arguments.
   *
   * Convert arguments from a JSON structure to an array.
   */
  protected function getArgs() {
    return !empty($this->config['arguments']) ? json_decode($this->config['arguments'], TRUE) : '';
  }

  /**
   * Override parent::getRaw().
   */
  public function getRaw() {
    $args = $this->getArgs();
    $client = $this->getClient();

    $result = $client->{$this->config['function']}($args);
    $xml = $result->{$this->config['function'] . 'Result'};

    return  $this->sanitizeRaw($xml);
  }

}

/**
 * Fetches data via a SOAP service.
 */
class FeedsSOAPFetcher extends FeedsFetcher {

  /**
   * Override parent::fetch().
   */
  public function fetch(FeedsSource $source) {
    $config = $this->getConfig();

    return new FeedsSOAPResult($config);
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'endpoint' => '',
      'wsdl' => TRUE,
      'namespace' => '',
      'use' => 'encoded',
      'function' => '',
      'arguments' => '',
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $config = $this->getConfig();

    $form = array();

    $form['endpoint'] = array(
      '#type' => 'textfield',
      '#title' => t('SOAP server endpoint URL'),
      '#maxlength' => 256,
      '#description' => t('The absolute endpoint URL of the SOAP server service. If WSDL is being used, this will be the URL to retrieve the WSDL.'),
      '#default_value' => $config['endpoint'],
      '#required' => TRUE
    );

    $form['wsdl'] = array(
      '#type'  => 'checkbox',
      '#title' => t('Use WSDL'),
      '#default_value' => $config['wsdl'],
    );

    $form['namespace'] = array(
      '#type' => 'textfield',
      '#title' => t('Target Namespace'),
      '#default_value' => $config['namespace'],
      '#maxlength' => 256,
      '#description' => t('Target namespace URI.'),
      '#states' => array(
        'visible' => array(
          ':input[name="wsdl"]' => array('checked' => FALSE),
        ),
        'required' => array(
          ':input[name="wsdl"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['use'] = array(
      '#type' => 'radios',
      '#title' => 'Use',
      '#default_value' => $config['use'],
      '#options' => array(
        'encoded' => t('Encoded'),
        'literal' => t('Literal'),
      ),
      '#description' => t('Specify how the SOAP client serializes the message.'),
      '#states' => array(
        'visible' => array(
          ':input[name="wsdl"]' => array('checked' => FALSE),
        ),
      ),
    );

    $form['function'] = array(
      '#type' => 'textfield',
      '#title' => t('SOAP Function'),
      '#maxlength' => 256,
      '#description' => t('Enter the function name to be called.'),
      '#default_value' => $config['function'],
      '#required' => TRUE
    );

    $form['arguments'] = array(
      '#type' => 'textarea',
      '#title' => t('Arguments'),
      '#rows' => 6,
      '#description' => t('Enter the arguments of the function in JSON format..'),
      '#default_value' => $config['arguments'],
    );

    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
    if (!empty($values['arguments']) && json_decode($values['arguments']) === NULL) {
      form_set_error('arguments', t('Invalid JSON syntax.'));
    }
  }

}
